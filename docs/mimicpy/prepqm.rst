PrepQM
======
As the name suggests, :code:`mimicpy prepqm` is the main tool to prepare MiMiC QM/MM input files. In short, it accepts MM topology and coordinate files, and after inputting the QM selection, outputs the CPMD input file and GROMACS index file necessary to create the TPR run file (or it can output the TPR run file directly).

An example of running a simple :code:`mimicpy prepqm` command in the console:

.. code-block:: console

	$ mimicpy prepqm -top acetone.top -coords acetone.gro


	                       ***** MiMiCPy *****

 	         For more information type mimicpy [subcommand] --help

	 =====> Running prepqm <=====


	 **Reading topology**

	 Cannot find path to Gromacs installation.
	 Read atoms from acetone.itp.
	 No atoms found in acetone.top.

	 Some atom types had no atom numbers information.
	 They were guessed as follows:

	 +---------------------+
	 | Atom Type | Element |
	 +---------------------+
	 |     c     |    C    |
	 +---------------------+
	 |     c3    |    C    |
	 +---------------------+
	 |     o     |    O    |
	 +---------------------+
	 |     hc    |    H    |
	 +---------------------+

	 **Reading coordinates**  |Done

	 Please enter selection below. For more information type 'help'
	 > add resname is ACT
	 > q
	 Using default values for maxstep and timestep
	 Wrote Gromacs index file to index.ndx
	 Wrote new CPMD input script to cpmd.inp

	 =====> Done <=====


Options
-------

:code:`mimicpy prepqm` has the following options:

Input Options
~~~~~~~~~~~~~
  -top [.top]
	The file name of the MM topology file. Currently, only GROMACS topology files are accepted.

  -coords [.gro/.pdb]
	The file name of the initial coordinates for the QM/MM run. Please make sure that the order of atoms matches exactly with that in the topology file.

  -mdp [.mdp]
	An optional GROMACS MD parameters file can be passed. If this is passed, :code:`mimicpy prepqm` will check the file for errors and consistency with the 	MiMiC run. It will then automatically generate the GROMACS run TPR file using the :code:`gmx grompp` tool.

Output Options
~~~~~~~~~~~~~~
  -out [.inp] (cpmd.inp)
	The file name of the output CPMD script. By default, it is :code:`cpmd.inp`.

  -ndx [.ndx] (index.ndx)
	The file name of the output GROMACS index file containing the GROMACS IDs of the QM atoms. This is necessary to generate a run TPR file. By default, 	the 	file name is :code:`index.ndx`.

  -tpr [.tpr] (mimic.tpr)
	If the GROMACS MD parameters file is passed, the TPR file will be generated with this file name. By default, it is generated as :code:`mimic.tpr`.

.. _prepqm other options:

Other Options
~~~~~~~~~~~~~
  -guess (True)
	CPMD requires the atomic species to be specified for each atom in the input script. This is sometimes not contained in the GROMACS topology, especially 	for non-standard atoms not present in the force field data. In these cases, MiMiCPy can guess the elements of each atom from other information present in 	the topology. This flag toggles this guessing of atomic elements. The elements are guessed using the following algorithm:

	1.	If the atomic mass of the atom is 1 a.m.u, it is assigned as a hydrogen.

	2.	If the atomic mass is between 1 and 36 a.m.u, the atomic number is approximated as half of the atomic mass.

	3.	If the atomic mass is greater than or equal to 36, the atomic name is checked to see if it corresponds with a valid atomic symbol.

	4.	If this is not true, the same is done with the atom type.

	5.  If this is also not true, the first letter of the atomic name is checked to see if it corresponds with a valid atomic symbol.

	6.  If this is also not true, the same is done for the atomic type.

	8.	Finally if this is also not true, the atom is assigned as hydrogen and a warning is printed.

	:code:`mimicpy prepqm` will print out the elements guessed in an easy to read format. This is an example for a non-standard acetone molecule:

	.. code-block:: console

		Some atom types had no atom numbers information.
		They were guessed as follows:

		+---------------------+
		| Atom Type | Element |
		+---------------------+
		|     c     |    C    |
		+---------------------+
		|     c3    |    C    |
		+---------------------+
		|     o     |    O    |
		+---------------------+
		|     hc    |    H    |
		+---------------------+

	Always cross-check that the elements guessed make physical sense. If this guessing is turned off, the program will exit with an error if it encounters an 	atom with no element information in the topology.

  -sele [.txt/.dat]
	The file name of the list of selection commands in text format. The selection commands to select the QM region atoms can be entered in the interactive 	session, or optionally passed as a file. The format of the selection keywords is given in the :ref:`following section<mimicpy inter sele>`.

  -pp [.dat]
	The file name of the text file containing the pseudopotential information for each atom element. The pseudopotential filename, the non-locatlity (LMAX, 	LOC) and other labels given as the header of each pseudopotential in the &ATOMS section of the CPMD input script can be specified in this file. MiMiCPy 	will use this information to automatically fill up the pseudopotential information. The data for each element is given in separate lines, and each line has 	the following format:

	:code:`<element> <pp filename> <link pp filename> <labels> <lmax> <loc>`

	A :code:`-` can be specified to skip an of this information. Below and example is given, detailing the pseudopotential information for zinc and carbon.

	.. code-block::

		Zn Zn_MT_BLYP_SEMI.psp - GAUSS-HERMITE=20 D S # Zinc atoms
		C C_MT_BLYP.psp C_GIA_DUM_AN_BLYP.oecp KLEINMAN-BYLANDER P - # Carbon atoms

	Characters specified after # are treated as comments and not read.

  -bound (False)
	Toggle automatic guessing of boundary atoms. By default, this is :code:`False`. It is important to specify boundary atoms at the QM-MM interface. Atoms can be marked as boundary atoms manually in the selection interface (see next section). Or MiMiCPy can automatically mark the QM atoms at the QM/MM interface as boundary atoms. If this is performed, :code:`prepqm` will output the atoms marked as :code:`link` in the format below:

	.. code-block:: console

		Link atoms were automatically set. The below atoms were marked as link:

		+------------------+
		|  Atom  | Residue |
		+------------------+
		|  82 CA |  5 HE2  |
		+------------------+
		| 286 CA |  18 HD5 |
		+------------------+
		| 255 CA |  16 HD4 |
		+------------------+

  -nsa [.txt/.dat]
	File name of file containing list of non-standard atom types in a two columns format. CPMD requires the element information for all QM atoms. If this is 	not present for some atom types in the topology, they can be specified in this file:

	:code:`<atom type> <element>`

	For example:

	.. code-block::

		c c
		c3 c
		o1 O

  -ff
	MiMiCPy requires access to the force field data directory to read the GROMACS topology. This is usually present in the :code:`GMXDATA` or :code:`GMXLIB` 	environment labels (in a standard GROMACS installation). If this is not found, MiMiCPy checks the current directory for the force field folder. To override 	this behavior, pass the directory containing the force field folder with this flag.

  -inp [.inp]
	File name of the template CPMD input script. By default, :code:`mimicpy prepqm` outputs a barebones CPMD script, with only the :code:`&MIMIC` and 	:code:`&ATOMS` sections filled. To have a complete input script, place the other CPMD sections and commands in this template script.

  -gmx (gmx)
	If the GROMACS MD parameters file is passed, :code:`mimicpy prepqm` will attempt to run the GROMACS preprocessor to generate a TPR file. Specify the name 	of the executable for running GROMACS (for example, :code:`gmx`, :code:`gmx_mpi` or :code:`gmx_mpi_d`) with this flag.

  -pad (0)
	In MiMiC, the QM region is treated as an isolated system. This often requires the use of Poisson solver like the Tuckerman solver, where a padding should 	be given between the outermost QM atoms and the QM box wall. This value can be specified as a float in nanometers with this flag. The value is added in each direction of 	the box size, in both direction (i.e., multiplied by 2).

  -abs (False)
	In a MiMiC run, the :code:`CELL` size in the CPMD script must be specified in the relative format. But you can force the the cell size to be written in 	the absolute format with this flag.

  -qma (QMatoms)
	The name of the QM atoms group in the GROMACS index file. This name must be passed correctly in the .mdp file. If the GROMACS MD parameters file is passed, 	this is checked automatically.

  -path
	The path to the GROMACS TPR file must be specified in the :code:`&MIMIC` section of the CPMD script. By default, :code:`mimicpy prepqm` uses the current 	directory. This can be overridden by passing a custom directory with this flag.

  -q
	Specify the total charge of the QM region. By default, MiMiCPy calculates this by using the MM charges of the QM atoms from the force field data. Passing 	this flag overrides this behavior.

  -buf (1000)
	Buffer size for reading input topology. Should only be used if unexpected errors occur in reading of topology.

  -bufc (1000)
	Buffer size for reading input coordinates. Should only be used if unexpected errors occur in reading of coordinates.

.. _mimicpy inter sele:

The Interactive Selection Environment
-------------------------------------

After :code:`mimicpy preqm` successfully reads the given input options, the user will be taken into the interactive selection environment. Here, the atoms to be added to the QM region can be specified using the MiMiCPy selection language. Details on the selection language can be found in the :ref:`following section<mimicpy sele lang>`. The following are valid commands:

add <selection>
	The :code:`add` command, followed by a selection command, can be used to add atoms to the QM region. Here is an example:
	:code:`add resname is ACT`
	Any errors with the selection command are displayed below.

add-link <selection>
	The :code:`add-link` command, followed by a selection command, can be used to link atoms to the QM region. Here is an example:
	:code:`delete resname is ACT`
	Any errors with the selection command are displayed below. Link atoms will be separated into their own pseudopotential block in the :code:`&ATOMS` section 	of the CPMD input file.

delete <selection>
	The :code:`delete` command, followed by a selection command, can be used to delete/remove atoms from the QM region. Here is an example:
	:code:`delete resname is ACT`
	Any errors with the selection command are displayed below.

clear
	The :code:`clear` command completely clears the QM region, and deletes all selected QM atoms.

view <filename>
	The :code:`view` command, optionally followed by a text filename, can be used to display the details of the QM atoms selected. If a filename is specified, 	the details are written to file. If not, they are written to the console window.

q or quit
	The :code:`q` or the :code:`quit` command causes :code:`mimicpy prepqm` to exit from the interactive environment. All QM selected will be used to generate 	the QM and MM input scripts.

.. _mimicpy sele lang:

The Selection Language
~~~~~~~~~~~~~~~~~~~~~~

The syntax for the selection query involves the following general structure:

.. code-block::

	keyword logical_operator value

where :code:`keyword` can include the following:

:resname: The residue name of the selection according to the MM topology
:resid: The residue ID of the selection according to the MM topology
:name: The atom name of the selection according to the MM topology
:type: The atom type of the selection according to the MM topology
:id: The atom ID of the selection according to the order they appear in the MM coordinates
:mol: The molecule of the selection according to the MM topology

and :code:`logical_operator` can be :code:`is`, :code:`not`, :code:`>`, :code:`<`, :code:`>=`, or :code:`<=`.

Different selection queries can be joined by using and or or operators, and grouped with brackets, such as:

.. code-block::

	add (resname is RES) and (name is x)


which will add the atom named X of residue RES.


VMD and PyMOL Plugins
---------------------

The console version of the :code:`PrepQM` tool allows for selecting the QM region with only the MiMiCPy selection language. This is sufficient for simple selection commands. But, if more complex QM regions are required, :code:`PrepQM` plugins for the popular molecular visualization software VMD and PyMOL are provided. Many of the same options available in the console version, are available in the plugins as well. Refer to the previous section for a detailed explanation on these options. Before running :code:`PrepQM` in the command link window of the visualization software, the coordinate file containing the input atomic coordinates for the MiMiC run must be loaded.

PyMOL Plugin
~~~~~~~~~~~~

The usage and options for the :code:`PrepQM` PyMOL plugin is given below:

.. code-block::

	prepqm top, selection [, link_selection [, ndx [, out [, inp [, pad [, abs [, qma [, path [, q [, pp [, link]]]]]]]]]]]

:top: The file name of the MM topology file. Currently, only GROMACS topology files are accepted.
:selection: The PyMOL selection expression or name pattern of the QM region.
:link_selection: Optional PyMOL selection expression or name pattern of the link atoms in the QM region.
:ndx: The file name of the output GROMACS index file containing the GROMACS IDs of the QM atoms. {default: :code:`index.ndx`}
:out: The file name of the output CPMD script. {default: :code:`cpmd.inp`}
:inp: File name of the template CPMD input script. Performs a similar function to the console version.
:pad: The padding to be added to the QM region in nanometers. The value is added in each direction of the box size, in both direction (i.e., multiplied by 2). {default: 0}
:abs: Force the the cell size to be written in the absolute format with this flag. {default: False}
:qma: The name of the QM atoms group in the GROMACS index file.  {default: qmatoms}
:path: The path to the GROMACS TPR file must be specified in the :code:`&MIMIC` section of the CPMD script.
:q: Specify the total charge of the QM region.
:pp: The file name of the text file containing the pseudopotential information for each atom element. Performs a similar function to the console version.
:link: Toggle automatically guessing of link atoms. {default: True}

An example of running a simple :code:`prepqm` command in the PyMOL command line:

.. code-block:: console

	prepqm acetone.top, sele0, pad=3.5, link=True

VMD Plugin
~~~~~~~~~~

The usage and options for the :code:`PrepQM` VMD plugin is given below:

.. code-block::

	prepqm [top topol.top] [sele atomselect0] [sele_link None] [molid 0] [ndx index.ndx] [out cpmd.inp] [inp None] [pad 0] [abs False] [qma qmatoms] [path 	None]  [q 0] [pp None] [find_link False]


:top: The file name of the MM topology file. Currently, only GROMACS topology files are accepted. {default: :code:`topol.top`}
:sele: The PyMOL selection expression or name pattern of the QM region. {default: atomselect0}
:sele_link: Optional PyMOL selection expression or name pattern of the link atoms in the QM region.
:molid: The molecular ID of the coordinate file loaded into VMD.
:ndx: The file name of the output GROMACS index file containing the GROMACS IDs of the QM atoms. {default: :code:`index.ndx`}
:out: The file name of the output CPMD script. {default: :code:`cpmd.inp`}
:inp: File name of the template CPMD input script. Performs a similar function to the console version.
:pad: The padding to be added to the QM region in nanometers. The value is added in each direction of the box size, in both direction (i.e., multiplied by 2). {default: 0}
:abs: Force the the cell size to be written in the absolute format with this flag. {default: False}
:qma: The name of the QM atoms group in the GROMACS index file.  {default: qmatoms}
:path: The path to the GROMACS TPR file must be specified in the :code:`&MIMIC` section of the CPMD script.
:q: Specify the total charge of the QM region.
:pp: The file name of the text file containing the pseudopotential information for each atom element. Performs a similar function to the console version.
:link: Toggle automatically guessing of link atoms. {default: True}

An example of running a simple :code:`prepqm` command in the VMD command line:

.. code-block:: console

	prepqm -top acetone.top -sele sele0 -pad 3.5 -link True
