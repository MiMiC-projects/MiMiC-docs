.. _external-programs:

External Programs
=================

Currently, the only way to enable MiMiC in the supported programs comes in the
form of patches, which must be applied to the original source codes.

The following instructions mostly follow the compilation of the original
software. Therefore, if you come across any problems unrelated to MiMiC, please
consult their respective manuals.

Following the instructions for MiMiC, we will install the programs in the
``.local`` directory in the user's home, but feel free to choose whichever
location you prefer::

  INSTALL_DIR=${HOME}/.local

Set the environment variables ``FC``, ``CC``, and ``CXX`` to your chosen
Fortran, C, and C++ compilers, respectively. We will also use the same
directory for the sources as in the instructions for MiMiC, but use
whichever location you prefer::

  SOURCE_DIR=${HOME}/mimic_sources

CPMD
----

In your ``${SOURCE_DIR}`` directory, download and unpack CPMD 4.3::

  cd ${SOURCE_DIR}
  wget -O cpmd-4.3.tar.gz https://github.com/CPMD-code/CPMD/archive/refs/tags/4.3.tar.gz
  tar -zxvf cpmd-4.3.tar.gz

Download the :download:`MiMiC patch for CPMD <../patches/MiMiC_CPMD-4.3.sh>`,
copy it to the CPMD root directory, and apply the patch::

  cp MiMiC_CPMD-4.3.sh ./CPMD-4.3/
  cd CPMD-4.3
  bash MiMiC_CPMD-4.3.sh

If you want to enable support for PLUMED in CPMD, then follow the instructions
in :ref:`plumed-cpmd` before continuing.

.. _configure-cpmd:

To make the install path available within CPMD's configuration script you first
have to export the ``INSTALL_DIR`` variable::

  export INSTALL_DIR

Choose a configuration file from the ``configure`` directory that is most
suitable for your target system (you may need to edit it to make it work).
Append the following two lines to the selected configuration file::

  FFLAGS=${FFLAGS}" -I${INSTALL_DIR}/include "
  LFLAGS=${LFLAGS}" -L${INSTALL_DIR}/lib -L${INSTALL_DIR}/lib64 "

They should be the last two lines in the configuration file. If you use
GCC then add also ``-fallow-argument-mismatch -ffree-line-length-none`` to
``FFLAGS``.

.. attention::

    Make sure that your selected configuration **supports MPI**, as this is
    required by MiMiC. There are no further checks and you would discover the
    incompatibility during runtime.

Now configure the build using the ``configure.sh`` script, switch to the build
directory, and compile::

  bash ./configure.sh -DEST=build -omp -mimic <configuration-file-name>
  cd build
  make

Optionally, as a final touch you can copy the CPMD executable to
``INSTALL_DIR``::

  mkdir -p ${INSTALL_DIR}/bin
  cp ${SOURCE_DIR}/CPMD-4.3/build/bin/cpmd.x ${INSTALL_DIR}/bin/

.. admonition:: Pseudopotentials

    Pseudopotentials are not distributed with CPMD, so do not forget to
    download them either from the CPMD website or your favourite
    pseudopotential repository.

.. _plumed-cpmd:

Enabling support for PLUMED
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To enable support for PLUMED in CPMD, there are a few additional steps. First
you need to have PLUMED installed. If you already have a working PLUMED
installation, you can skip the PLUMED installation part.

.. note::

    We have tested PLUMED v2.6.5, v2.7.4, v2.8.0, and v2.8.1, but it is likely
    that other versions of PLUMED 2 are also compatible with the CPMD patch
    provided in the MiMiC tarball.

.. admonition:: Installing PLUMED

    Here we provide a quick installation guide for PLUMED. For more detailed
    information please consult the official
    `PLUMED documentation <https://www.plumed.org/doc-v2.8/user-doc/html/_installation.html>`_.

    First move to the source directory, download PLUMED v2.8.1 from the official
    `PLUMED website <https://www.plumed.org/download>`_, and unpack it::

      cd ${SOURCE_DIR}
      wget https://github.com/plumed/plumed2/releases/download/v2.8.1/plumed-2.8.1.tgz
      tar -zxvf plumed-2.8.1.tgz

    Then enter the PLUMED source directory, configure, build, and install::

      cd plumed-2.8.1
      ./configure CXX=<c++-mpi-wrapper> --prefix=${INSTALL_DIR}
      make
      make install

    Finally, set the following environment variables::

      export PATH=${INSTALL_DIR}/bin:${PATH}
      export LIBRARY_PATH=${INSTALL_DIR}/lib:${LIBRARY_PATH}
      export LD_LIBRARY_PATH=${INSTALL_DIR}/lib:${LD_LIBRARY_PATH}
      export PLUMED_KERNEL=${INSTALL_DIR}/lib/libplumedKernel.so
      export PLUMED_VIMPATH=${INSTALL_DIR}/lib/plumed/vim
      export PYTHONPATH=${INSTALL_DIR}/lib/plumed/python:${PYTHONPATH}

First, ensure that the CPMD source has been patched with the MiMiC patch by following the instructions above. Then download
the :download:`PLUMED patch for CPMD <../patches/PLUMED_CPMD-4.3.sh>`, copy it
to the CPMD root directory, and apply it::

  cp PLUMED_CPMD-4.3.sh ./CPMD-4.3/
  cd CPMD-4.3
  bash PLUMED_CPMD-4.3.sh

Next, use PLUMED to patch CPMD::

  plumed patch -n CPMD-4.3
  plumed patch -p -e CPMD-4.3

This step creates the ``Plumed.inc``, ``Plumed.h``, and ``Plumed.cmake`` files
which are necessary to compile CPMD with PLUMED support.

Finally, add the ``-plumed`` option in the :ref:`configuration step <configure-cpmd>`.


GROMACS
-------
.. We recommend that you use the latest GROMACS 2021 release. Note that the
   following instructions assume version 2021.6. Replace the version number
   if you downloaded a different version. In your ``${SOURCE_DIR}`` directory,
   download and unpack the latest GROMACS 2021 from the official
   `GROMACS website <https://manual.gromacs.org/2021-current/index.html>`_::

In your ``${SOURCE_DIR}`` directory, download and unpack GROMACS 2021.6 from
the official
`GROMACS website <https://manual.gromacs.org/2021.6/download.html>`_::

  cd ${SOURCE_DIR}
  wget -O gromacs-2021.6.tar.gz https://ftp.gromacs.org/gromacs/gromacs-2021.6.tar.gz
  tar -zxvf gromacs-2021.6.tar.gz

Download the
:download:`MiMiC patch for GROMACS <../patches/MiMiC_GROMACS-2021.patch>`,
copy it to the GROMACS directory, and apply it::

  cp MiMiC_GROMACS-2021.patch gromacs-2021.6/
  cd gromacs-2021.6
  patch -p1 < MiMiC_GROMACS-2021.patch

Create a build directory, enter it, configure the build by running CMake,
build, and finally install::

  mkdir build
  cd build
  cmake -DCMAKE_BUILD_TYPE=Release -DGMX_DOUBLE=ON -DGMX_MPI=ON -DGMX_MIMIC=ON \
        -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DCMAKE_PREFIX_PATH=${INSTALL_DIR} ..
  make
  make install

You are free to use any other flags, but those shown here are required for a
successful build of MiMiC-enabled GROMACS.
