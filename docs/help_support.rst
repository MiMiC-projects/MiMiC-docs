.. _faq:

Frequently Asked Questions (FAQ)
================================

For now, there are no questions.

.. _help:

How to Get Help
===============

MiMiC is distributed for free and without any guarantee of reliability, accuracy, or
suitability for any particular purpose. No obligation to provide technical support is
expressed or implied.

We have a `discussion group`_ for our user community. We encourage you to be comprehensive
in providing explanations and to share all relevant background information that could
facilitate better assistance. Please be aware that although we strive to assist you to
the best of our ability, we are not equipped to offer support for external programs, such
as CPMD and GROMACS. For bug reports, please visit our GitLab_ page.

Where should you post?
----------------------

Before you decide to seek help, please make sure that your issue is not a common one
by consulting the :ref:`FAQ <faq>`.

* **How do I ...?** Ask in the `discussion group`_ or file an issue on GitLab_
* **I get this error, why?** Ask in the `discussion group`_ or file an issue on GitLab_
* **I get this error and I am sure it is a bug.** Please file an issue on GitLab_
* **I have an idea/request.** Ask in the `discussion group`_ or file an issue on GitLab_
* **I have an idea/request and a plan for its implementation.** Please file an issue on GitLab_
* **Why do you ...?** Ask in the `discussion group`_ or file an issue on GitLab_
* **When will you ...?** Ask in the `discussion group`_ or file an issue on GitLab_
* **Can I open a discussion on this part of the code?** Please file an issue on GitLab_

.. _GitLab: https://gitlab.com/mimic-project/user-support/-/issues

.. _discussion group: https://groups.google.com/g/mimic-project

.. _known-issues:

Known Issues
============

In this section, a list of known issues in the latest version of MiMiC is reported, together with
suggestions for workarounds. Each issue is reported using the following structure when applicable:

* **Description**: Brief description of the problem.
* **Identification**: Explanation of the "symptoms" or scenarios in which users might encounter the issue.
* **Workaround**: Suggestions on how to address the issue until it is fixed in MiMiC.

Constraints in GROMACS
----------------------

* **Description**: GROMACS allows the user to specify a variety of constraints in a `.mdp` file.
  In particular, constraining bond with hydrogens is common using the keyword `constraints = h-bonds`.
  However, this functionality is not currently supported in MiMiC.
* **Identification**: Using `constraints = h-bonds` will cause problems in the constraints solver,
  resulting in an unphysical elongation of bonds that include hydrogens.
* **Workaround**: Use `constraints = none` in the `.mdp` file. If needed, specify all constraints for
  your system in the CPMD input (CONSTRAINT keyword in the &ATOM section; check the CPMD manual for more details).
  Note that the atom indices need to be the ones in CPMD. For this purpose, MiMiCPy CPMDid_ can be helpful.

.. _CPMDid: https://mimic-project.org/en/latest/mimicpy/other_commands.html#cpmdid

Discontinuities in energy and temperature
-----------------------------------------

* **Description**: Energy and temperature may present discontinuities when the dynamic load balancing is
  turned off in GROMACS.
* **Identification**: From visual inspection of the energy or temperature, it is possible to observe "jumps",
  after which those quantities keep evolving "unperturbed". To confirm that those discontinuities
  are related to this issue (and not, for example, to a badly equilibrated system), it is possible to check
  the GROMACS `.log` file: a message like *step N Turning off dynamic load balancing, because it is degrading
  performance* will be printed, and the step number *N* will correspond to the step where the discontinuity
  point can be observed in the energy during the trajectory.
* **Workaround**: Dynamic load balancing can be turned off in GROMACS `mdrun` with the option `-dlb no`.
  Turning off dynamic load balancing in GROMACS is not expected to have a significant impact on the
  performance in a MiMiC-based QM/MM MD simulation.

Available water models
----------------------

Currently, MiMiC only supports three-site water models (e.g., TIP3P, SPC), both rigid and flexible.
Water models with more than three sites (e.g. TIP4P, TIP5P) are currently not supported.
